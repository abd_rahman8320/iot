<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Revenue extends Model
{
    protected $fillable = [
        'account_name', 'billing_cycle', 'billable', 'customer_type', 'billing_address', 'operator_account_id',
        'devices', 'data_volume', 'sms_volume', 'voice_volume', 'csd_volume', 'subscription_charge',
        'currency', 'data_charge', 'sms_charge', 'voice_charge', 'csd_charge', 'activation_charge',
        'other_charge', 'fixed_discount_target', 'total_charge', 'account_type'
    ];
}
