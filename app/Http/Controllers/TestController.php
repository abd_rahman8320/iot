<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RawRevenue;
use App\Revenue;

class TestController extends Controller
{
    public function index()
    {
        ini_set('max_execution_time', 86400);

        foreach (RawRevenue::where('billable', 'Yes')->cursor() as $data) {
            $billing_cycle = explode(' ', $data->billing_cycle);
            $month = date('m', strtotime($billing_cycle[0]));
            $year = $billing_cycle[1];

            $voice_volume = explode(':', $data->voice_volume);
            $voice_volume = intval($voice_volume[0]*60) + intval($voice_volume[1]);

            $csd_volume = explode(':', $data->csd_volume);
            $csd_volume = intval($csd_volume[0]*60) + intval($csd_volume[1]);

            $revenue = Revenue::updateOrCreate(
                ['account_name' => $data->account_name, 'billing_cycle' => $year.'-'.$month.'-01'],
                [
                    'billable' => $data->billable,
                    'customer_type' => $data->customer_type,
                    'billing_address' => $data->billing_address,
                    'operator_account_id' => $data->operator_account_id,
                    'devices' => $data->devices,
                    'data_volume' => $data->data_volume,
                    'sms_volume' => $data->sms_volume,
                    'voice_volume' => $voice_volume,
                    'csd_volume' => $csd_volume,
                    'subscription_charge' => $data->subscription_charge,
                    'currency' => $data->currency,
                    'data_charge' => $data->data_charge,
                    'sms_charge' => $data->sms_charge,
                    'voice_charge' => $data->voice_charge,
                    'csd_charge' => $data->csd_charge,
                    'activation_charge' => $data->activation_charge,
                    'other_charge' => $data->other_charge,
                    'fixed_discount_target' => $data->fixed_discount_target,
                    'total_charge' => $data->total_charge,
                    'account_type' => $data->account_type
                ]
            );
        }
    }
}
