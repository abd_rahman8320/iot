<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRawRevenuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('raw_revenues', function (Blueprint $table) {
            $table->string('account_name');
            $table->string('billing_cycle');
            $table->string('billable');
            $table->string('customer_type');
            $table->text('billing_address');
            $table->string('operator_account_id');
            $table->integer('devices');
            $table->double('data_volume')->comment('in MB');
            $table->integer('sms_volume')->comment('in message');
            $table->string('voice_volume')->comment('mm:ss');
            $table->string('csd_volume')->comment('mm:ss');
            $table->double('subscription_charge');
            $table->string('currency');
            $table->double('data_charge');
            $table->double('sms_charge');
            $table->double('voice_charge');
            $table->double('csd_charge');
            $table->double('activation_charge');
            $table->double('other_charge');
            $table->double('fixed_discount_target');
            $table->double('total_charge');
            $table->string('account_type');
            $table->timestamps();
            $table->primary(['account_name', 'billing_cycle']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('raw_revenues');
    }
}
