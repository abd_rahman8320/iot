<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRevenuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('revenues', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('account_name');
            $table->date('billing_cycle');
            $table->string('billable');
            $table->string('customer_type');
            $table->text('billing_address');
            $table->string('operator_account_id');
            $table->integer('devices');
            $table->double('data_volume')->comment('in MB');
            $table->integer('sms_volume')->comment('in message');
            $table->integer('voice_volume')->comment('in seconds');
            $table->integer('csd_volume')->comment('in seconds');
            $table->double('subscription_charge');
            $table->string('currency');
            $table->double('data_charge');
            $table->double('sms_charge');
            $table->double('voice_charge');
            $table->double('csd_charge');
            $table->double('activation_charge');
            $table->double('other_charge');
            $table->double('fixed_discount_target');
            $table->double('total_charge');
            $table->string('account_type');
            $table->unique(['account_name', 'billing_cycle']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('revenues');
    }
}
